package com.example.pawe.mydiary;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotesFragment extends Fragment {
    RecyclerView recyclerView;
    public AdapteNotes adapterNotes;

    public NotesFragment() {
        // Required empty public constructor
    }

    public static NotesFragment getInstance(){

        return new NotesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notes, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        adapterNotes = new AdapteNotes(((MainActivity) getActivity()).noteStorage.notes);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapterNotes);
        //dzięki czemu v wysokosc recyclerView nie jest dynamiczna
        recyclerView.setHasFixedSize(true);
        return  view;
    }

}
