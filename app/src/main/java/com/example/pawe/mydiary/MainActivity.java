package com.example.pawe.mydiary;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    NoteStorage noteStorage;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //znalezienie coordinatorLayout po id
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        sharedPreferences = getSharedPreferences("com.example.pawe.mydiary", MODE_PRIVATE);
        /*
        to jest po to by kolejne fragmenty nie byly dodawane po przekreceniu telefonu,
        pomimo tego, że one już ta by były
         */
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.container, NotesFragment.getInstance(), "note_list");
            fragmentTransaction.commit();
        }
        noteStorage = new Gson().fromJson(sharedPreferences.getString("notes", ""), NoteStorage.class);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onClickFab(View view) {
        final AlertDialog.Builder alertDialogBuild = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuild.setTitle("Add new note");
        View inflate = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_add, null, false);
        final EditText editText = (EditText) inflate.findViewById(R.id.inputNote);
        alertDialogBuild.setView(inflate);
        alertDialogBuild.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                String input = editText.getText().toString();
                addNewNote(input);
                dialogInterface.dismiss();
            }
        });
        alertDialogBuild.show();
    }

    public void addNewNote(String text) {
        // używam Gsona do oddania notatek do sharedPreferences
        Gson gson = new Gson();
        NoteStorage notes = gson.fromJson(sharedPreferences.getString("notes", ""), NoteStorage.class);
        if (notes == null) {
            notes = new NoteStorage();
        }
        notes.notes.add(new Note(text));
        //linijka ponizej sprawia, ze bedzie aktualizowane (~)
        this.noteStorage = notes;
        //dzieki wyszukiwaniu przez tag mozemy znalezc fragment, ktory stworzylismy wczesniej i dalismy mu tag "note_list"
        NotesFragment note_list = (NotesFragment) getSupportFragmentManager().findFragmentByTag("note_list");
        // dzięki temu v zaktualizujemy liste fragmentu do najnowszej listy, ktorą dysponuje main_activity
        note_list.adapterNotes.notes = noteStorage.notes;
        //informujemy fragment, ze jego dane zostaly zmienione
        note_list.adapterNotes.notifyDataSetChanged();
        //snackbar, jest czyms co zastepuje toasta(wyswietlanie na ekranie informacji),
        // tylko inaczej wyglada i troche inaczej działa,
        // ma zdecydowanie więcej funkcji niż toast i tak jakby jest dla niego przyszłością
        Snackbar.make(coordinatorLayout, "Note added", Snackbar.LENGTH_SHORT).show();
        //zapisuję notatki do Jsona v i daje to możliwosc, ze bedzie nastepnym razem odczytany
        sharedPreferences.edit().putString("notes", gson.toJson(notes)).apply();
    }
}
