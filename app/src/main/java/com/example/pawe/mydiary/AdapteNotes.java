package com.example.pawe.mydiary;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paweł on 2017-10-08.
 */

public class AdapteNotes extends RecyclerView.Adapter<AdapteNotes.ViewHolder> {

    List<Note> notes = new ArrayList<>();

    public AdapteNotes(List<Note> notes) {
        this.notes = notes;
    }

    @Override
    //zostal stworzony adapter do listy
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        AdapteNotes.ViewHolder vh = new ViewHolder(inflate);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewContent.setText(notes.get(position).content);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewContent;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewContent = (TextView) itemView.findViewById(R.id.content);
        }
    }
}
